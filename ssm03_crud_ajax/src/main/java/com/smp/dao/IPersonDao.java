package com.smp.dao;

import com.smp.domain.Person;

import java.util.List;

public interface IPersonDao {
    //增删改查
    //add save
    //find findById findAll
    //update updateById
    //deleteById daleteByIds

    //select * from person;
    List<Person> findAll();

    //insert into person (name,money)values (?,?)
    void save(Person person);
}
