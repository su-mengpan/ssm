package com.smp.controller;

import com.smp.domain.Department;
import com.smp.domain.Result;
import com.smp.service.IDepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/deptv2")
public class DepartmentV2Controller {
    private static final Logger l= LoggerFactory.getLogger(DepartmentV2Controller.class);
    @Autowired
    //controller调用service层
    private IDepartmentService departmentService;
    /*@RequestMapping(path="/xx",method=  GET   POST代表请求页面)
     public 返回值类型 方法名(参数){ //页面提交过来的请求参数
        //..
        返回值类型决定返回给浏览器的内容
    }
     */
    //返回list界面
    @RequestMapping(path = "/listUI",method = RequestMethod.GET)
    public String listUI(){

        return "list_depts";
    }
    //地址带UI表示打开界面，不代表是返回数据
    @RequestMapping(path = "/list",method = RequestMethod.GET)
    //将list调jackson转成json字符串
    public @ResponseBody Object list(){
        //业务逻辑  调用查找所有部门的方法
        List<Department> list=departmentService.findAllDepartments();
        //返回对象需要被转成json字符串
        return Result.init(200,"",list);
    }
    //删除
    @RequestMapping(path = "/delete",method = RequestMethod.GET)
    public @ResponseBody Object delete(Integer did){
        //获取did，执行对该记录的删除
        l.info("delete did="+did);
        try {
            departmentService.deleteDepartmentById(did);
            return Result.init(200,"删除成功",null);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.init(-200,"删除失败",null);
    }
    //保存一般是使用post提交
    @RequestMapping(path = "/find",method = RequestMethod.GET)
    public @ResponseBody Object find(Integer did){
        //打印数据
        l.info("find did="+did);
        if(did!=null){
            //查询回显需要的数据
            Department dept=departmentService.findDepartmentById(did);
            if(dept!=null){
                //转成json返回界面
                return Result.init(200,null,dept);
            }
        }
        return Result.init(-200,"没有查询结果",null);
    }
    //更新
    @RequestMapping(path = "/update",method = RequestMethod.POST)
    @ResponseBody Object update(Department dept){
        l.info("update dept="+dept);
        try {
            departmentService.updateDepartmentById(dept);
            return Result.init(200,"更新成功",null);
        }catch (Exception e){
            e.printStackTrace();
        }
        return Result.init(-200,"更新失败",null);
    }
}
