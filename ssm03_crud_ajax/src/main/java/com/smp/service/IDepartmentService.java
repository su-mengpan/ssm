package com.smp.service;

import com.smp.domain.Department;

import java.util.List;

public interface IDepartmentService{
    //查找所有部门
    List<Department> findAllDepartments();
    //添加一个新的部门
    void saveDepartment(Department dept);
    //删除指定id的部门
    void deleteDepartmentById(int id);
    //根据id修改名称
    void updateDepartmentById(Department dept);
    //查找指定id的部门数据
    Department findDepartmentById(int did);
}
