package com.smp.service;

import com.smp.domain.Department;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestDepartmentService {
    private static final Logger l= LoggerFactory.getLogger(TestDepartmentService.class);
    @Autowired
    IDepartmentService service;
    @Test
    public void test01(){
        List<Department> list=service.findAllDepartments();
        l.info("test01 test="+list);

    }
    @Test
    public void test02(){
        Department dept=new Department("UI设计部门");
        service.saveDepartment(dept);
    }
    @Test
    public void test03(){
        service.deleteDepartmentById(1);
    }
    @Test
    public void test04(){
        Department dept=new Department();
        dept.setDid(3);
        dept.setDname("最牛部门");
        service.updateDepartmentById(dept);
    }
    @Test
    public void test05(){
        Department dept=service.findDepartmentById(1);
        l.info("test05 dept="+dept);
    }

}
