package com.smp.domain;

public class User {
    //用户id
    private int uid;
    //用户名
    private String uname;
    //密码
    private String upasswd;
    //电话号码
    private String uphone;
    //邮箱
    private String uemail;
    //权限
    private String upower;

    public User() {
    }

    public User(int uid, String uname, String upasswd, String uphone, String uemail, String upower) {
        this.uid = uid;
        this.uname = uname;
        this.upasswd = upasswd;
        this.uphone = uphone;
        this.uemail = uemail;
        this.upower = upower;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", upasswd='" + upasswd + '\'' +
                ", uphone='" + uphone + '\'' +
                ", uemail='" + uemail + '\'' +
                ", upower='" + upower + '\'' +
                '}';
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUpasswd() {
        return upasswd;
    }

    public void setUpasswd(String upasswd) {
        this.upasswd = upasswd;
    }

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public String getUemail() {
        return uemail;
    }

    public void setUemail(String uemail) {
        this.uemail = uemail;
    }

    public String getUpower() {
        return upower;
    }

    public void setUpower(String upower) {
        this.upower = upower;
    }
}
