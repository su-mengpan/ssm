package com.smp.service;

import com.smp.domain.User;

import java.util.List;

//接口
public interface IUserService {

    //查找所有，返回集合
    List<User> findAll();

    //保存用户
    void saveUser(User user);

    void saveUsers(List<User> userList);

    /*int login(User user);

    User findUserByUname(String uname);*/
}
