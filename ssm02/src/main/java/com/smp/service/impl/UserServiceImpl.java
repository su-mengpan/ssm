package com.smp.service.impl;

import com.smp.dao.IUserDao;
import com.smp.domain.User;
import com.smp.service.IUserService;
import com.smp.util.MySessionUtils2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
//实现接口service层
public class UserServiceImpl implements IUserService {
    @Autowired
    //定义一个私有dao
    private IUserDao iuserDao;
    @Override
    public List<User> findAll() {
        //定义一个集合连接dao层
        //查找所有
        List<User> list=iuserDao.findAll();
        return list;
    }

    @Override
    //保存所有
    public void saveUser(User user) {
        iuserDao.save(user);
    }

    @Override
    public void saveUsers(List<User> userList) {
    }

    /*@Override
    public int login(User user) {
        //账号密码
        IUserDao userDao = MySessionUtils2.getSession().getMapper(IUserDao.class);
        User u = userDao.findByUname(user.getUname());
        //比对数据库的账号密码
        if (u == null) {
            return -1;//找不到jack
        } else {
                if (u.getUname().equals(user.getUname()) && u.getUpasswd().equals(user.getUpasswd())) {
                    return 1;//提示登录成功
                } else {
                    return -2;//账号或者密码出错
            }
        }
    }

    @Override
    public User findUserByUname(String uname) {

        return null;
    }*/


}
