package com.smp.controller;

import com.smp.domain.User;
import com.smp.service.IUserService;
import org.apache.taglibs.standard.extra.spath.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;

    @RequestMapping(path ="/list",method = RequestMethod.GET)
    public String list(Model model){
        //显示所有的User数据
        List<User> list=userService.findAll();
        System.out.println("list() list="+list);
        //数据放在model对象上，有model传给页面
        model.addAttribute("list",list);//参1：key 参2：value
        return "list";
    }
}
