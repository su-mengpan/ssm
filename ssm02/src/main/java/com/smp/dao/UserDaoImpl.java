package com.smp.dao;

import com.smp.domain.User;

import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements IUserDao {
    @Override
    public List<User> findAll() {
        //验证可以输出findAll dao
        System.out.println("findAll dao");
        //定义一个新集合
        // ArrayList  基于Object[]实现的，输出为集合
        List<User> list = new ArrayList<>();
        //来一个for循环，输出相同的数值
        for (int i = 0; i < 10; i++) {
            User u = new User(2, "lisi", "123456", "123454534564", "2263574372@qq.com", "学生");
        }
        //输出为集合
        return list;
    }

    @Override
    public void save(User user) {
        //验证可以输出save dao
        System.out.println("save dao");
    }

}
