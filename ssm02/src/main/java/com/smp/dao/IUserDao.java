package com.smp.dao;

import com.smp.domain.User;

import java.util.List;
//dao层接口
public interface IUserDao {
    /*在这里写增删改查
    add insert
    delete
    update
    find  query*/

    //查找所有
    //select * from user;
    List<User> findAll();
    //插入数据
    //insert into user (uname,upasswd,uphone,uemail,upower) values (?,?,?,?,?)
    void save(User user);

    /*//根据姓名查找-->登录功能
    //select * from user where uname='zhsangsan'
    User findByUname(String uname);*/

}
