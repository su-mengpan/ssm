package com.smp.service;

import com.smp.dao.IUserDao;
import com.smp.domain.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestMyBatis {
    private SqlSession session;
    @Before
    public void init() throws IOException{
        InputStream in=TestMyBatis.class.getClassLoader().getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory factory=new SqlSessionFactoryBuilder().build(in);
        session=factory.openSession();
    }
    @After
    public void destory(){
        session.commit();
        session.close();
    }
    @Test
    public void test01(){
        IUserDao dao=session.getMapper(IUserDao.class);
        List<User> list=dao.findAll();
        System.out.println(list);
    }
    @Test
    public void test02(){
        IUserDao dao=session.getMapper(IUserDao.class);
        dao.save(new User(2019026,"zhangsan","123456","123454534564","2263574372@qq.com","学生"));
    }
}
