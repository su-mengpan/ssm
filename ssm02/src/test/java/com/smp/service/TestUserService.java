package com.smp.service;

import com.smp.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

//junit4测试驱动
@RunWith(SpringJUnit4ClassRunner.class)
//在class路径下找配置文件
@ContextConfiguration("classpath:applicationContext.xml")
public class TestUserService {
    //Logger:日志记录器、封装类
    private static final Logger log=Logger.getLogger(TestUserService.class.getName());
    //自动调用接口
    @Autowired
    //定义一个接口
    IUserService userService;
    @Test
    //编写两个功能
    public void test01(){
        //调用查询
        log.info(userService+"");
        //查找所有，返回list集合-----1
        List<User> list=userService.findAll();
        //调用保存1
        log.info(list+"");
        User user=new User(1,"jack","123456","123454534564","2263574372@qq.com","学生");
        //保存-----2
        userService.saveUser(user);

        //查找所有，返回-----3
        List<User> list1=userService.findAll();
        //调用保存2
        log.info(list1+"");
    }
    @Test
    public void test02(){
        //
        System.out.println(userService);
    }
    @Test
    public void test03(){
        List<User> userList=new ArrayList<>();
        userList.add(new User(5,"555","123456","123454534564","2263574372@qq.com","学生"));
        userList.add(new User(6,"666","123456","123454534564","2263574372@qq.com","学生"));
        userList.add(new User(7,"777","123456","123454534564","2263574372@qq.com","学生"));
        userService.saveUsers(userList);
    }
   /* @Test
    public void test04(){
        //将用户输入账号与密码发到后台

        //1:查找用户数据
        User user = userService.findUserByUname("zhangsan");
        //2:根据数据， 正确，错误，不存在。
        if (user == null) {
            System.out.println("不存在");
        } else {
            //1:3种情况
            System.out.println("存在");
        }
    }
    @Test
    public void test05(){
        //将用户输入账号与密码发到后台
        //1:查找用户数据
        User user = new User();
        user.setUname("zhangsan");
        user.setUpasswd("123456");
        int code = userService.login(user);
        //2:根据数据， 正确，错误，不存在。
        System.out.println(code);
    }*/
}
