package com.smp.service.impl;
import com.smp.dao.IPersonDao;
import com.smp.domain.Person;
import com.smp.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonServiceImpl implements IPersonService {

    @Autowired
    private IPersonDao iPersonDao;
    @Override
    public List<Person> findAll() {
        List<Person> list=iPersonDao.findAll();
        return list;
    }


    @Override
    public void savePerson(Person person) {
        iPersonDao.save(person);
    }

    @Override
    public void savePersons(List<Person> personList) {

    }

}
