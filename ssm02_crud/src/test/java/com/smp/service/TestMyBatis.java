package com.smp.service;

import com.smp.dao.IPersonDao;
import com.smp.domain.Person;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestMyBatis {
    //最核心对象是session
    private SqlSession session;
    @Before
    public void init() throws IOException {
        // 加载配置文件
        InputStream in = TestMyBatis.class.getClassLoader().getResourceAsStream("SqlMapConfig.xml");
        // 创建SqlSessionFactory对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(in);
        // 创建SqlSession对象
        session = factory.openSession();
    }
    @After
    public void destory(){
        session.commit();
        session.close();
    }
    @Test
    public void test01(){
        //查找所有的person
        IPersonDao dao=session.getMapper(IPersonDao.class);
        List<Person> list=dao.findAll();
        System.out.println(list);
    }


    @Test
    public void test02(){
        //保存新person
        IPersonDao dao=session.getMapper(IPersonDao.class);
        dao.save(new Person("tony",200.00));
    }

}