package com.smp.service;

import com.smp.domain.Person;

import java.util.List;

public interface IPersonService {
    //显示person数据
    List<Person> findAll();
    //保存person数据
    void savePerson(Person person);

    void savePersons(List<Person> personList);
}
