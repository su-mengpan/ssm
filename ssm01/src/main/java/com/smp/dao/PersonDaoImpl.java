package com.smp.dao;

import com.smp.dao.IPersonDao;
import com.smp.domain.Person;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
//@Repository 此类以后讲不用 将由Mybatis自动生成

public class PersonDaoImpl implements IPersonDao {
    @Override
    public List<Person> findAll() {
        System.out.println("findAll dao");
        List<Person> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Person p = new Person("jack" + i, 100.00);
        }
        return list;
    }

    @Override
    public void save(Person person) {
        System.out.println("save dao");
    }
}
